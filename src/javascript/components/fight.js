import { controls } from '../../constants/controls';


export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over

    const leftFighterIndicator = document.getElementById('left-fighter-indicator');
    const rightFighterIndicator = document.getElementById('right-fighter-indicator');

    leftFighterIndicator.style.width = '100%';
    rightFighterIndicator.style.width = '100%';

    const states = {
      PlayerOne: { lastTimeCriticalHit: null, IsBlock: false, health: firstFighter.health },
      PlayerTwo: { lastTimeCriticalHit: null, IsBlock: false, health: secondFighter.health },
    };

    const pressedKeysCriticalHitFirst = new Set();
    const pressedKeysCriticalHitSecond = new Set();

    document.addEventListener('keydown', function (event) {
      const currentAction = event.code;

      switch (currentAction) {
        case controls.PlayerOneAttack:
          if (!states.PlayerTwo.IsBlock && !states.PlayerOne.IsBlock) {
            states.PlayerTwo.health -= getDamage(firstFighter, secondFighter);
            states.PlayerTwo.health = states.PlayerTwo.health < 0 ? 0 : states.PlayerTwo.health;
            rightFighterIndicator.style.width = (100 * states.PlayerTwo.health) / secondFighter.health + '%';
          }
          break;
        case controls.PlayerOneBlock:
          states.PlayerOne.IsBlock = true;
          break;
        case controls.PlayerTwoAttack:
          if (!states.PlayerOne.IsBlock && !states.PlayerTwo.IsBlock) {
            states.PlayerOne.health -= getDamage(secondFighter, firstFighter);
            states.PlayerOne.health = states.PlayerOne.health < 0 ? 0 : states.PlayerOne.health;
            leftFighterIndicator.style.width = (100 * states.PlayerOne.health) / firstFighter.health + '%';
          }
          break;
        case controls.PlayerTwoBlock:
          states.PlayerTwo.IsBlock = true;
          break;

        default:
          break;
      }

      if ([...controls.PlayerOneCriticalHitCombination].some((it) => it == currentAction)) {
        pressedKeysCriticalHitFirst.add(currentAction);
      } else if ([...controls.PlayerTwoCriticalHitCombination].some((it) => it == currentAction)) {
        pressedKeysCriticalHitSecond.add(currentAction);
      }

      if (
        pressedKeysCriticalHitFirst.size === 3 &&
        [...pressedKeysCriticalHitFirst].every((it) => controls.PlayerOneCriticalHitCombination.includes(it))
      ) {
        if (!states.PlayerOne.IsBlock && CriticalHit(states.PlayerOne.lastTimeCriticalHit)) {
          states.PlayerTwo.health -= getCriticalDamage(firstFighter);
          states.PlayerTwo.health = states.PlayerTwo.health < 0 ? 0 : states.PlayerTwo.health;
          rightFighterIndicator.style.width = (100 * states.PlayerTwo.health) / secondFighter.health + '%';
          states.PlayerOne.lastTimeCriticalHit = new Date();
        }
        pressedKeysCriticalHitFirst.clear();
      } else if (
        pressedKeysCriticalHitSecond.size === 3 &&
        [...pressedKeysCriticalHitSecond].every((it) => controls.PlayerTwoCriticalHitCombination.includes(it))
      ) {
        if (!states.PlayerTwo.IsBlock && CriticalHit(states.PlayerTwo.lastTimeCriticalHit)) {
          states.PlayerOne.health -= getCriticalDamage(secondFighter);
          states.PlayerOne.health = states.PlayerOne.health < 0 ? 0 : states.PlayerOne.health;
          leftFighterIndicator.style.width = (100 * states.PlayerOne.health) / firstFighter.health + '%';
          states.PlayerTwo.lastTimeCriticalHit = new Date();
        }
        pressedKeysCriticalHitSecond.clear();
      }

      if (states.PlayerOne.health <= 0) {
        resolve(secondFighter);
      } else if (states.PlayerTwo.health <= 0) {
        resolve(firstFighter);
      }
    });

    document.addEventListener('keyup', function (event) {
      const currentAction = event.code;

      switch (currentAction) {
        case controls.PlayerOneBlock:
          states.PlayerOne.IsBlock = false;
          break;

        case controls.PlayerTwoBlock:
          states.PlayerTwo.IsBlock = false;
          break;

        default:
          break;
      }

      if ([...pressedKeysCriticalHitFirst].some((it) => it == currentAction)) {
        pressedKeysCriticalHitFirst.delete(currentAction);
      } else if ([...pressedKeysCriticalHitSecond].some((it) => it == currentAction)) {
        pressedKeysCriticalHitSecond.delete(currentAction);
      }
    });

  });
}

function CriticalHit(lastTimeCriticalHit) {
  return lastTimeCriticalHit ? Math.floor((new Date() - lastTimeCriticalHit) / 1000) > 10 : true;
}

export function getCriticalDamage(fighter) {
  const { attack } = fighter;
  return 2 * attack;
}

export function getDamage(attacker, defender) {
  // return damage
  const hitPower = getHitPower(attacker);
  const blockPower = getBlockPower(defender);
  return blockPower > hitPower ? 0 : hitPower - blockPower;

}

export function getHitPower(fighter) {
  // return hit power
  const { attack } = fighter;
  const criticalHitChance = Math.random() + 1;
  return attack * criticalHitChance;

}

export function getBlockPower(fighter) {
  // return block power
  const { defense } = fighter;
  const dodgeChance = Math.random() + 1;
  return defense * dodgeChance;

}